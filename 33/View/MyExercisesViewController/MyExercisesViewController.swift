//
//  ViewController.swift
//  33
//
//  Created by VanjaPin on 22.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//

import UIKit

protocol MyExercisesViewControllerProtocol: class {
    
    func setEmptySate() -> Void
    func removeEmptyState() -> Void
    func updateTableView() -> Void
}

class MyExercisesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addExerciseButton: UIControl!
    
    var modelView: MyExercisesModelViewProtocol! {
        didSet {
            modelView.viewModelDidChanged = { [weak self] viewModel in
                guard case let strongSelf = self else { return }
                if viewModel.numberOfMyExercises() > 0 {
                    strongSelf!.removeEmptyState()
                    strongSelf!.updateTableView()
                } else {
                    strongSelf!.setEmptySate()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        viewDidLoad()
        title = modelView.title
        
       tableView.tableFooterView = UIView(frame: CGRect.zero)
       tableView.tableHeaderView = UIView(frame: CGRect.zero)
        
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewWillAppear(animated)
        
        modelView.updateTableViewIfNeeded()
        
    }
    
    //MARK: - Private
    
    private func prepareUI() -> Void {
        modelView.updateUI()
        addExerciseButton.layer.cornerRadius = addExerciseButton.bounds.height / 2.0
        addExerciseButton.clipsToBounds = true
    }
    
    private func removeEmptyState() {
       tableView.isHidden=false
    }
    
    private func setEmptySate() -> Void {
        tableView.isHidden=true
    }
    
    private func updateTableView() -> Void {
        tableView.reloadData()
    }
    
    //MARK: - Action
    
    @IBAction func tapToAddButton(_ sender: Any) {
        RouterService.shared.presentAllExercisesViewController()
    }
    
}

extension MyExercisesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelView.numberOfMyExercises()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExerciseTableViewCell.reuseIdentifier) as! ExerciseTableViewCell
        cell.modelView = modelView.getExerciseModelView(at: indexPath.row)
        
        return cell
    }
    
}

extension MyExercisesViewController: UITableViewDelegate {
    
    static let visibleCellInTable:CGFloat = 9
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / MyExercisesViewController.visibleCellInTable
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            modelView.removeExercise(byIndex: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .bottom)
            updateTableView()
        }
    }
    
}

