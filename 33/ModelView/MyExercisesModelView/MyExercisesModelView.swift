//
//  MyExercisesModelView.swift
//  33
//
//  Created by VanjaPin on 22.05.2020.
//  Copyright © 2020 VanjaPin. All rights reserved.
//
import Foundation

protocol MyExercisesModelViewProtocol: class {
    
    var title: String { get }
    var viewModelDidChanged: ((MyExercisesModelViewProtocol) -> Void)? {get set}
    
    func numberOfMyExercises() -> Int
    func updateUI() -> Void
    func updateTableViewIfNeeded() -> Void
    func getExerciseModelView(at index: Int) -> ExerciseModelView
    func removeExercise(byIndex index: Int) -> Void
    
}

class MyExercisesModelView: NSObject, MyExercisesModelViewProtocol {
    
    var title: String {
        get {
            return "My Exercises"
        }
    }
    var viewModelDidChanged: ((MyExercisesModelViewProtocol) -> Void)?
    
    private var myExercises: [ExerciseModel]
    
    override init() {
        self.myExercises = []
        super.init()
    }

    func updateUI() {
        self.viewModelDidChanged?(self)
    }
    
    func updateTableViewIfNeeded() {
       
            self.viewModelDidChanged?(self)
        
    }
    
    func numberOfMyExercises() -> Int {
        return myExercises.count
    }
    
    func getExerciseModelView(at index: Int) -> ExerciseModelView {
        return ExerciseModelView(exerciseModel: myExercises[index])
    }
    
    func removeExercise(byIndex index: Int) -> Void {
         myExercises.remove(at: index)
       
        
        for i in index ..< myExercises.count {
            myExercises[i].index -= 1
        }
    }
    
}
